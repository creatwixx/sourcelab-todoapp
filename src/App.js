import './App.css';
import Form from './components/Form';
import TodoList from './components/TodoList';
import { useEffect, useState } from 'react';

function App() {
  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState('all');
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(() => {
    fetchTodos();
  }, []);

  const fetchTodos = async () => {
    await fetch(`http://localhost:3000/todo`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => Promise.all([res.status, res.json()]))
      .then(([status, data]) => {
        if (status === 404) {
          return Promise.reject(data.message);
        }
        return data;
      })
      .then((data) => {
        setTodos(data);
      })
      .catch((e) => console.log(e));
  };

  const onCreateTodo = (todo) => {
    fetch(`http://localhost:3000/todo`, {
      body: JSON.stringify({
        title: todo.title,
      }),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => Promise.all([res.status, res.json()]))
      .then(([status, createdTodo]) => {
        if (status === 404) {
          return Promise.reject(createdTodo.message);
        }
        if (status === 400) {
          return Promise.reject(createdTodo.message);
        }
        return createdTodo;
      })
      .then((createdTodo) => setTodos([...todos, createdTodo]))
      .catch((e) => console.log(e));
  };

  const onDeleteTodo = (todoId) => {
    fetch(`http://localhost:3000/todo/${todoId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => Promise.all([res.status, res.json()]))
      .then(([status, deletedTodo]) => {
        if (status === 404) {
          return Promise.reject(deletedTodo.message);
        }
        return deletedTodo;
      })
      .then((deletedTodo) => {
        const filteredEmployees = todos.filter(
          (todo) => todo.id !== deletedTodo.id
        );

        setTodos(filteredEmployees);
      })
      .catch((e) => console.log(e));
  };

  const onUpdateTodo = (todoId) => {
    return fetch(`http://localhost:3000/todo/${todoId}`, {
      method: 'PATCH',
      body: JSON.stringify({ completed: true }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => Promise.all([res.status, res.json()]))
      .then(([status, updatedTodo]) => {
        if (status === 404) {
          return Promise.reject(updatedTodo.message);
        }
        if (status === 400) {
          return Promise.reject(updatedTodo.message);
        }
        return updatedTodo;
      })
      .then((updatedTodo) => {
        console.log(updatedTodo);
        const updatedTodos = todos.map((todo) =>
          todo.id === updatedTodo.id ? updatedTodo : todo
        );
        setTodos(updatedTodos);
      })
      .catch((e) => console.log(e));
  };

  useEffect(() => {
    switch (status) {
      case 'completed':
        setFilteredTodos(todos.filter((todo) => todo.completed));
        break;
      case 'uncompleted':
        setFilteredTodos(todos.filter((todo) => !todo.completed));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  }, [todos, status]);

  return (
    <div className='App'>
      <header>
        <h1>SourceLab ToDo List</h1>
      </header>
      <Form onCreateTodo={onCreateTodo} setStatus={setStatus} />

      {todos.length === 0 ? (
        <div className='todo-container'>You dont have anything to do!</div>
      ) : (
        <TodoList
          todos={filteredTodos}
          onDeleteTodo={onDeleteTodo}
          onUpdateTodo={onUpdateTodo}
        />
      )}
    </div>
  );
}

export default App;
