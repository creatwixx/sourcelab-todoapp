import React from 'react';

function Todo({ todo, onDeleteTodo, onUpdateTodo }) {
  return (
    <div className='todo'>
      <li className={`todo-item ${todo.completed ? 'completed' : ''}`}>
        {todo.title}
      </li>
      <button className='complete-btn' onClick={() => onUpdateTodo(todo.id)}>
        <i className='fas fa-check'></i>
      </button>
      <button className='trash-btn' onClick={() => onDeleteTodo(todo.id)}>
        <i className='fas fa-trash'></i>
      </button>
    </div>
  );
}

export default Todo;
