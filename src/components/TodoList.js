import React from 'react';
import Todo from './Todo';

function TodoList({ todos, onDeleteTodo, onUpdateTodo }) {
  return (
    <div className='todo-container'>
      <ul className='todo-list'>
        {todos.map((todo) => (
          <Todo
            key={todo.id}
            todo = {todo}
            onDeleteTodo={onDeleteTodo}
            onUpdateTodo={onUpdateTodo}
          />
        ))}
      </ul>
    </div>
  );
}

export default TodoList;
