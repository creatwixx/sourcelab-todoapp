import React from 'react';
import { useForm } from 'react-hook-form';

function Form({ onCreateTodo, setStatus }) {
  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm();

  const createTodoHandler = (form) => {
    onCreateTodo(form);
    setValue('title', '');
  };

  const statusHandler = (e) => {
    setStatus(e.target.value);
  };

  return (
    <form onSubmit={handleSubmit(createTodoHandler)}>
      <input
        placeholder={errors.title && 'Title is required!'}
        type='text'
        className='todo-input'
        {...register('title', {
          required: true,
        })}
      />
      <button className='todo-button' type='submit'>
        <i className='fa fa-plus-square' aria-hidden='true'></i>
      </button>

      <div className='select'>
        <select onChange={statusHandler} name='todos' className='filter-todo'>
          <option value='all'>All</option>
          <option value='completed'>Completed</option>
          <option value='uncompleted'>Uncompleted</option>
        </select>
      </div>
    </form>
  );
}

export default Form;
